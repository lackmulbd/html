<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'travpart' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o:X&%@r8k=CBzqwPzTa-DXN1t(JSEL7Wj>aiOi2]K&^<*.:Hz`_vuRO`?m/V+8lY' );
define( 'SECURE_AUTH_KEY',  '.2-0|A6$JYwxz:~Mw%eSzl?19-O4}kRp:j$$Zqlz;SYtcko-&G|fh:w)y<!{y_a+' );
define( 'LOGGED_IN_KEY',    'U{/[MmOCZzWTHU;+v1OheLWH* zcd$2vOZLifoWgbT/fgY+U03HmQdZ)2D@R`@@b' );
define( 'NONCE_KEY',        '=8,VnnHQWCg-Q/xYNf{[7i4w`k2aH1PieQBtm0Qj)zfe>~Wbo,,L6]HgAa5N*x@-' );
define( 'AUTH_SALT',        '{v#TQ),R)YSr,SzyLnl$E6i[cG{HB]%^l~?~PQm{,~`7o?|(Y_,?9W!FC Joy&$:' );
define( 'SECURE_AUTH_SALT', '`KYW:L4[Kg)bi4A^_3 [W}@uGZsz,#EjIYB^=;301dDtOO]Z~zVkU]rjQ1<y`;(L' );
define( 'LOGGED_IN_SALT',   'YUDF_`[[~a9j }gQ8F+O6-FRT/IRSuz)-8M*MLEqrm*wI=N.q(_;l-z;A1Xlip&H' );
define( 'NONCE_SALT',       '-o}4wmT!8}k|nXc^B(ZfarK|u $MKtua*}s]^HniE#&&Iv=3ogW {pt:7[}6Ot|?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
