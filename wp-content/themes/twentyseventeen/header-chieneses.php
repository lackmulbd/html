<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header " role="banner">
		<div class="container">
        <div class="row">
        	<div class="col-md-4">
            	<div class="logo">
                	<a href="http://www.travpart.com/Chinese/"><img src="http://www.travpart.com/wp-content/uploads/2018/08/logo.jpg" /></a>
                </div>
            </div>
            <div class="col-md-8">
            <div class="language-combinator">
            	<a href="http://www.travpart.com/"><img src="http://www.travpart.com/wp-content/uploads/2018/08/english-1.png" /><span>English</span></a>
            </div>
            </div>
            </div>
        </div><!--end container-->
        
        <div class="divider-row"></div>
        <div class="container">
         <div class="below-logo">
		<div style="width:70%;">
		<ul class="list-unstyle header-logo">
		<li class="ttl-1-li"><a class="ttl-1" href="http://www.travpart.com/Chinese/travcust/" >Customize Travel</a>
		<a class="ttl-1-logo below-logo-fl" href="http://www.travpart.com/Chinese/travcust/"><img src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_5d530297-1236-49a1-8d98-13dadb59abc0.jpg" /></a>
		</li>
		<li class="ttl-2-li"><a class="ttl-2" href="http://www.travpart.com/Chinese/travchat/">Chat with Our Travel Agents</a>
		<a href="http://www.travpart.com/Chinese/travchat/" class="below-logo-fl ttl-2-logo"><img src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_1b312699-b11a-40bb-805e-0ba4a25a30c4.jpg" /></a></li>
		</ul>
		<div>
		<div style="width:50%;float:left;"></div>
		<div style="width:50%;float:right;"></div>
		<span class="float:none;clear:both;"></span>
		</div>
		</div>
       <!-- <span>
        	<a class="ttl-1" href="http://www.travpart.com/English/travcust" >Customize Travel</a>
<a class="ttl-1-logo .below-logo-fl" href="http://www.travpart.com/English/travcust"><img src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_5d530297-1236-49a1-8d98-13dadb59abc0.jpg" /></a>
            </span>
            <span>
            <a class="ttl-2" href="http://www.travpart.com/English/travchat">Chat with Our Travel Agents</a>
            <a href="http://www.travpart.com/English/travchat" class="below-logo-fl ttl-2-logo"><img src="http://www.travpart.com/wp-content/uploads/2018/08/C_Users_ilham_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_1b312699-b11a-40bb-805e-0ba4a25a30c4.jpg" /></a>
            </span>-->
        </div>
        </div>
       
		<?php get_template_part( 'template-parts/header/header', 'image' ); ?>

		<?php if ( has_nav_menu( 'top' ) ) : ?>
			<div class="navigation-top">
				<div class="wrap">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div><!-- .wrap -->
			</div><!-- .navigation-top -->
		<?php endif; ?>

	</header><!-- #masthead -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">